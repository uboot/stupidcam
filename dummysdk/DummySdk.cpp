#include <boost/config.hpp>
#include <stupidcam/CameraSdkPlugin.h>

namespace dummysdk {

class DummySdkPlugin : public stupidcam::CameraSdkPlugin
{
public:
    virtual const std::list<stupidcam::Camera*> cameras() override { return std::list<stupidcam::Camera*>(); }
};

extern "C" BOOST_SYMBOL_EXPORT DummySdkPlugin plugin;
DummySdkPlugin plugin;

} // dummysdk