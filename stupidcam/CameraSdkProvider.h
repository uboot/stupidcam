#ifndef STUPIDCAM_CAMERASDKPROVIDER_H
#define STUPIDCAM_CAMERASDKPROVIDER_H

#include <list>

#include "stupidcam/CameraSdkPlugin.h"

namespace stupidcam
{

class CameraSdkProvider
{
public:
    virtual const std::list<CameraSdkPlugin*> cameraSdks() = 0;
    virtual void load() = 0;
};

}

#endif // STUPIDCAM_CAMERASDKPROVIDER_H
