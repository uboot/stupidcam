#include "stupidcam/CameraList.h"

#include <boost/assert.hpp>

namespace stupidcam
{

CameraList* CameraList::m_instance = nullptr;

CameraList & CameraList::instance()
{
    if (m_instance)
    {
        return *m_instance;
    }

    m_instance = new CameraList;
    return *m_instance;
}

void CameraList::load()
{
    if (m_sdkProvider)
    {
        m_sdkProvider->load();
        auto sdks = m_sdkProvider->cameraSdks();
        for (auto sdk : sdks)
        {
            for (auto camera : sdk->cameras())
            {
                m_cameras.push_back(camera);
            }
        }
    }
}

}
