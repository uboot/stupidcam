#ifndef STUPIDCAM_CAMERALIST_H
#define STUPIDCAM_CAMERALIST_H

#include "stupidcam/Camera.h"
#include "stupidcam/CameraSdkProvider.h"

#include <list>

namespace stupidcam
{

class CameraList final
{
    friend class CameraListTest;
public:
    CameraList() = default;
    void setCameraSdkProvider(CameraSdkProvider* const provider) { m_sdkProvider = provider; }

    static CameraList & instance();
    void load();
    const std::list<Camera*> & cameras() const { return m_cameras; }

private:

    static CameraList* m_instance;

    std::list<Camera*> m_cameras;
    CameraSdkProvider* m_sdkProvider = nullptr;
};

}

#endif // STUPIDCAM_CAMERALIST_H
