#ifndef STUPIDCAM_CAMERASDKPLUGIN_H
#define STUPIDCAM_CAMERASDKPLUGIN_H

#include <list>

#include "stupidcam/Camera.h"

namespace stupidcam
{

class CameraSdkPlugin
{
public:
    virtual const std::list<stupidcam::Camera*> cameras() = 0;
};

}

#endif // STUPIDCAM_CAMERASDKPLUGIN_H
