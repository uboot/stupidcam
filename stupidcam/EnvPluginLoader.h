#ifndef STUPIDCAM_ENVPLUGINLOADER_H
#define STUPIDCAM_ENVPLUGINLOADER_H

#include <list>
#include <boost/shared_ptr.hpp>

#include "stupidcam/CameraSdkProvider.h"

namespace stupidcam
{

class EnvPluginLoader : public CameraSdkProvider
{
public:
    virtual const std::list<CameraSdkPlugin*> cameraSdks() override;
    virtual void load();

    void loadPlugin(const std::string & pluginPath);

private:
    typedef boost::shared_ptr<CameraSdkPlugin> CameraSdkPluginPtr;

    std::list<CameraSdkPluginPtr> m_plugins;
};

}

#endif // STUPIDCAM_ENVPLUGINLOADER_H
