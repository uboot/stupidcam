#include "stupidcam/CameraList.h"

#include <gmock/gmock.h>

#include <string>

namespace {

// The fixture for testing class Foo.
class CameraListTest : public ::testing::Test {
protected:
    // You can remove any or all of the following functions if its body
    // is empty.

    CameraListTest() {
        // You can do set-up work for each test here.
    }

    virtual ~CameraListTest() {
        // You can do clean-up work that doesn't throw exceptions here.
    }

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    virtual void SetUp() {
        // Code here will be called immediately after the constructor (right
        // before each test).
    }

    virtual void TearDown() {
        // Code here will be called immediately after each test (right
        // before the destructor).
    }

    // Objects declared here can be used by all tests in the test case for Foo.
    stupidcam::CameraList cameraList;
};

class MockCameraSdkProvider : public stupidcam::CameraSdkProvider
{
public:
    MOCK_METHOD0(cameraSdks, const std::list<stupidcam::CameraSdkPlugin*>());
    MOCK_METHOD0(load, void());
};

class MockCameraSdkPlugin : public stupidcam::CameraSdkPlugin
{
public:
    MOCK_METHOD0(cameras, const std::list<stupidcam::Camera*>());
};

class MockCamera : public stupidcam::Camera
{
};


TEST_F(CameraListTest, LoadSdks) 
{
    MockCameraSdkProvider sdks;
    MockCameraSdkPlugin plugin;
    std::list<stupidcam::CameraSdkPlugin*> plugins{ &plugin };
    MockCamera camera;
    std::list<stupidcam::Camera*> cameras{ &camera };

    EXPECT_CALL(sdks, load()).Times(1);
    EXPECT_CALL(sdks, cameraSdks()).WillOnce(::testing::Return(plugins));
    EXPECT_CALL(plugin, cameras()).WillOnce(::testing::Return(cameras));

    cameraList.setCameraSdkProvider(&sdks);
    cameraList.load();
    EXPECT_EQ(1u, cameraList.cameras().size());
}

}  // namespace
