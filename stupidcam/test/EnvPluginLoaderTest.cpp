#include "stupidcam/EnvPluginLoader.h"

#include <gmock/gmock.h>

#include <stdlib.h>
#include <string>

namespace {

class EnvPluginLoaderTest : public ::testing::Test {
protected:
    ~EnvPluginLoaderTest()
    {
        unsetenv("STUPIDCAM_PLUGINS");
    }
};

TEST_F(EnvPluginLoaderTest, LoadPlugin)
{
    stupidcam::EnvPluginLoader loader;
    loader.loadPlugin(std::string(DUMMY_SDK));

    EXPECT_EQ(1u, loader.cameraSdks().size());
}

TEST_F(EnvPluginLoaderTest, LoadPluginsFromEnv)
{
    stupidcam::EnvPluginLoader loader;
    setenv("STUPIDCAM_PLUGINS", DUMMY_SDK, true);
    loader.load();

    EXPECT_EQ(1u, loader.cameraSdks().size());
}

TEST_F(EnvPluginLoaderTest, LoadPluginsFromEmptyEnv)
{
    stupidcam::EnvPluginLoader loader;
    loader.load();

    EXPECT_EQ(0u, loader.cameraSdks().size());
}

}  // namespace
