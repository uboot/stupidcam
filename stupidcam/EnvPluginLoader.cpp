#include "stupidcam/EnvPluginLoader.h"

#include <sstream>
#include <boost/dll.hpp>

namespace stupidcam
{

const std::list<CameraSdkPlugin*> EnvPluginLoader::cameraSdks()
{
    std::list<CameraSdkPlugin*> list;

    for(CameraSdkPluginPtr plugin : m_plugins)
    {
        list.push_back(plugin.get());
    }

    return list;
}

void EnvPluginLoader::load()
{
    const char* plugins = getenv("STUPIDCAM_PLUGINS");
    if (!plugins)
    {
        return;
    }

    std::istringstream stream(plugins);
    std::string pluginPath;

    while (std::getline(stream, pluginPath, ':'))
    {
        loadPlugin(pluginPath);
    }
}

void EnvPluginLoader::loadPlugin(const std::string & pluginPath)
{
    boost::shared_ptr<CameraSdkPlugin> plugin = nullptr;

    plugin = boost::dll::import<CameraSdkPlugin>(
        pluginPath,
        "plugin",
        boost::dll::load_mode::append_decorations
        );

    m_plugins.push_back(plugin);
}

}